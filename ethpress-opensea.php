<?php
/**
 * Plugin Name:     EthPress OpenSea
 * Plugin URI:      https://gitlab.com/losnappas/ethpress-opensea
 * Description:     OpenSea ERC-721 and ERC-1155 support for EthPress.
 * Author:          Lynn (lynn.mvp at tutanota dot com)
 * Author URI:      https://www.imlynn.xyz/
 * Text Domain:     ethpress_opensea
 * Domain Path:     /languages
 * Version:         1.1.1
 *
 * @package         Ethpress_Opensea
 */

namespace losnappas\Ethpress_Opensea;

defined( 'ABSPATH' ) || die;

require_once 'vendor/autoload.php';

define( 'ETHPRESS_OPENSEA_FILE', __FILE__ );
define( 'ETHPRESS_OPENSEA_NS', __NAMESPACE__ );
define( 'ETHPRESS_OPENSEA_PHP_MIN_VER', '5.4.0' );
define( 'ETHPRESS_OPENSEA_WP_MIN_VER', '4.6.0' );

if ( version_compare( \get_bloginfo( 'version' ), ETHPRESS_OPENSEA_WP_MIN_VER, '<' ) || version_compare( PHP_VERSION, ETHPRESS_OPENSEA_PHP_MIN_VER, '<' ) ) {
	/**
	 * Displays notification.
	 */
	function ethpress_opensea_compatability_warning() {
		echo '<div class="error"><p>' . esc_html(
			sprintf(
				/* translators: version numbers. */
				__( '“%1$s” requires PHP %2$s (or newer) and WordPress %3$s (or newer) to function properly. Your site is using PHP %4$s and WordPress %5$s. Please upgrade. The plugin has been automatically deactivated.', 'ethpress_opensea' ),
				'EthPress OpenSea',
				ETHPRESS_OPENSEA_PHP_MIN_VER,
				ETHPRESS_OPENSEA_WP_MIN_VER,
				PHP_VERSION,
				$GLOBALS['wp_version']
			)
		) . '</p></div>';
		// phpcs:ignore -- no nonces here.
		if ( isset( $_GET['activate'] ) ) {
			// phpcs:ignore -- no nonces here.
			unset( $_GET['activate'] );
		}
	}
	add_action( 'admin_notices', __NAMESPACE__ . '\ethpress_opensea_compatability_warning' );

	/**
	 * Deactivates.
	 */
	function ethpress_opensea_deactivate_self() {
		deactivate_plugins( plugin_basename( ETHPRESS_OPENSEA_FILE ) );
	}
	add_action( 'admin_init', __NAMESPACE__ . '\ethpress_opensea_deactivate_self' );

	return;
} else {
	Plugin::attach_hooks();
}

