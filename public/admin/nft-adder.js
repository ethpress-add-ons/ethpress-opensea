/**
 * Adds fields for contract addresses and tokens ids in WC product NFT data tab
 *
 * On submit, it gets all the comma'd values and splits them up into
 * address:id, because that's how the OpenSea API needs them.
 *
 * @package Ethpress_Opensea
 * @since 0.1.0
 */
jQuery( function( $ ) {
	var $pdata = $( '#ethpress_opensea_product_data' ),
		contracts,
		tokens,
		both = {},
		key;
	if ( ! $pdata ) {
		return;
	}
	contracts = $( '#ethpress_opensea_contract_addresses' ).val();
	tokens = $( '#ethpress_opensea_token_ids' ).val();
	contracts = contracts.split( ',' );
	tokens = tokens.split( ',' );

	contracts.forEach( function( contract, idx ) {
		var token;
		if ( contract && contract.trim && contract.trim() ) {
			contract = contract.trim();
			if ( ! Array.isArray( both[contract]) ) {
				both[contract] = [];
			}
			token = tokens[idx] && tokens[idx].trim();
			if ( token ) {
				both[contract].push( token );
			}
		}
	});
	for ( key in both ) {
		addels( key, both[key].join( ',' ) );
	}

	$( '#ethpress_opensea_add' ).click( function( e ) {
		e.preventDefault();
		addels( '', '' );
	});

	$( 'form#post' ).on( 'submit', function( e ) {
		var tokens = Array.from( document.querySelectorAll( '.ethpress_opensea_token_id' ) );
		var contracts = Array.from( document.querySelectorAll( '.ethpress_opensea_contract_address' ) );
		var t, i, token;
		$( '.ethpress_opensea_token_id' ).remove();
		$( '.ethpress_opensea_contract_address' ).remove();
		t = [];
		for ( i = 0; i < contracts.length; i++ ) {
			t = tokens[i].value.split( ',' );
			for ( token of t ) {
				addels( contracts[i].value, token );
			}
		}
		$( '#ethpress_opensea_contract_addresses' ).val(
			Array.from( document.querySelectorAll( '.ethpress_opensea_contract_address' ) ).map( function( item, idx ) {
				if ( 0 < idx ) {
					return ',' + $( item ).val();
				}
				return $( item ).val();
			})
		);
		$( '#ethpress_opensea_token_ids' ).val(
			Array.from( document.querySelectorAll( '.ethpress_opensea_token_id' ) ).map( function( item, idx ) {
				var value = $( item ).val();
				value = value.split( ',' );
				if ( 0 < idx ) {
					return ',' + $( item ).val();
				}
				return $( item ).val();
			})
		);
	});

	function addels( data1, data2 ) {
		var el, el2, div;
		div = document.createElement( 'span' );
		$( div ).attr( 'class', 'ethpress_opensea_fields_bundle' );
		el = document.createElement( 'input' );
		$( el ).attr( 'class', 'ethpress_opensea_contract_address' );
		$( el ).attr( 'placeholder', 'Contract Address' );
		$( el ).attr( 'type', 'text' );
		$( el ).val( data1.trim() );
		$( div ).append( el );

		el2 = document.createElement( 'input' );
		$( el2 ).attr( 'class', 'ethpress_opensea_token_id' );
		$( el2 ).attr( 'placeholder', 'Token ids separated with comma' );
		$( el2 ).val( data2.trim() );
		$( el2 ).attr( 'type', 'text' );
		$( div ).append( el2 );
		$( '#ethpress_opensea_show_items' ).append( div );
	}
});
