<?php
/**
 * Plugin's startup
 *
 * Specify addresses and ids in woocommerce Product Data: NFT Data fields.
 *
 * @package Ethpress_Opensea
 *
 * @since 0.1.0
 */

namespace losnappas\Ethpress_Opensea;

defined( 'ABSPATH' ) || die;

use losnappas\Ethpress\Address;

/**
 * Starts up the plugin and attaches hooks.
 *
 * @since 0.1.0
 */
class Plugin {

	/**
	 * Assets array returned from OpenSea, first populated on init
	 *
	 * @var array
	 * @link https://docs.opensea.io/reference#asset-object
	 * @since 1.1.0
	 */
	public static $ownerships = array();

	/**
	 * Hook the plugin.
	 *
	 * @since 0.1.0
	 */
	public static function attach_hooks() {
		// @since 1.1.0 -- Forgot it earlier.
		add_action( 'init', array( __CLASS__, 'load_plugin_textdomain' ) );

		add_filter(
			'woocommerce_product_data_tabs',
			array( __CLASS__, 'woocommerce_product_data_tabs' )
		);
		add_filter(
			'woocommerce_product_data_panels',
			array( __CLASS__, 'woocommerce_product_data_panels' )
		);
		add_action(
			'woocommerce_process_product_meta',
			array( __CLASS__, 'woocommerce_process_product_meta' )
		);
		add_action(
			'init',
			array( __CLASS__, 'init' )
		);
		add_filter(
			'the_content',
			array( __CLASS__, 'the_content' )
		);

		if ( is_admin() ) {
			add_action(
				'admin_head',
				array( __CLASS__, 'product_tab_icon' )
			);
			add_action(
				'admin_enqueue_scripts',
				array( __CLASS__, 'admin_enqueue_scripts' )
			);
			add_action(
				'admin_menu',
				array( ETHPRESS_OPENSEA_NS . '\Admin\Options', 'admin_menu' )
			);
			add_action(
				'admin_init',
				array( ETHPRESS_OPENSEA_NS . '\Admin\Options', 'admin_init' )
			);
			$plugin = plugin_basename( ETHPRESS_OPENSEA_FILE );
			add_filter( "plugin_action_links_$plugin", array( ETHPRESS_OPENSEA_NS . '\Admin\Options', 'plugin_action_links' ) );
		}
		add_action(
			'wp_enqueue_scripts',
			array( __CLASS__, 'wp_enqueue_scripts' )
		);
	}

	/**
	 * Changes woocommerce product tab icon for NFT into dollar.
	 *
	 * @since 0.1.0
	 */
	public static function product_tab_icon() {
		echo '<style>ul.wc-tabs .ethpress_opensea_NFT_tab a::before { content: "\f18e" !important; }</style>';
	}

	/**
	 * Hooked to init.
	 *
	 * @since 0.5.0
	 */
	public static function init() {
		add_shortcode(
			'ethpress_opensea_nft',
			array( ETHPRESS_OPENSEA_NS . '\Shortcodes\Nftlink', 'add_shortcode' )
		);
	}

	/**
	 * Enqueues the admin script.
	 *
	 * @since 0.1.0
	 *
	 * @param string $page Current page.
	 */
	public static function admin_enqueue_scripts( $page ) {
		if ( 'post.php' === $page || 'post-new.php' === $page ) {
			wp_enqueue_script(
				'ethpress_opensea',
				plugin_dir_url( ETHPRESS_OPENSEA_FILE ) . '/public/admin/nft-adder.js',
				array( 'jquery' ),
				'1',
				false
			);
		}
	}

	/**
	 * Enqueues required scripts.
	 *
	 * @since 0.1.0
	 */
	public static function wp_enqueue_scripts() {
		if ( ! is_user_logged_in() ) {
			wp_enqueue_script( 'ethpress-login-front' );
		}
	}

	/**
	 * Adds a woocommerce product data tab.
	 *
	 * @since 0.1.0
	 *
	 * @param Array $tabs Tabs array.
	 */
	public static function woocommerce_product_data_tabs( $tabs ) {
		$tabs['ethpress_opensea_NFT'] = array(
			'label'    => __( 'NFT Data', 'ethpress_opensea' ),
			'target'   => 'ethpress_opensea_product_data',
			'priority' => 21,
		);
		return $tabs;
	}

	/**
	 * Outputs fields to product data thing.
	 *
	 * @since 0.1.0
	 */
	public static function woocommerce_product_data_panels() {
		echo '<div id="ethpress_opensea_product_data" class="panel woocommerce_options_panel">';
		echo '<p>' . esc_html__( 'Require an NFT and restrict user access to the product page. Only users with the token will be able to view the product.', 'ethpress_opensea' ) . '</p>';
		echo '<p>' . esc_html__( 'Blank entries will be removed when post is updated.', 'ethpress_opensea' ) . '</p>';
		$meta = get_post_meta(
			get_the_ID(),
			'ethpress_opensea_product_data',
			true
		);
		if ( empty( $meta['contract_addresses'] ) ) {
			$meta = array(
				'redirect_url'       => '',
				'contract_addresses' => '',
				'token_ids'          => '',
			);
		}
		woocommerce_wp_text_input(
			array(
				'id'          => 'ethpress_opensea_redirect_url',
				'value'       => $meta['redirect_url'],
				'label'       => esc_html__( 'Redirect url', 'ethpress_opensea' ),
				'description' => esc_html__( 'Redirect user here if they do not own ONE OF the required tokens defined below.', 'ethpress_opensea' ),
				'desc_tip'    => true,
			)
		);
		woocommerce_wp_hidden_input(
			array(
				'id'          => 'ethpress_opensea_contract_addresses',
				'value'       => $meta['contract_addresses'],
				'label'       => esc_html__( 'NFT Contract Address', 'ethpress_opensea' ),
				'description' => esc_html__( 'A smart contract address.', 'ethpress_opensea' ),
			)
		);
		woocommerce_wp_hidden_input(
			array(
				'id'          => 'ethpress_opensea_token_ids',
				'value'       => $meta['token_ids'],
				'label'       => esc_html__( 'NFT Token ID', 'ethpress_opensea' ),
				'description' => esc_html__( 'The ID of the token.', 'ethpress_opensea' ),
			)
		);
		echo '<p class="form-field"><label>' . esc_html__( 'NFT', 'ethpress_opensea' ) . '</label>';
		echo '<span style="margin: auto; width: 95%;" id="ethpress_opensea_show_items">';
		echo '</span>';
		echo '<span style="padding-top:10px" class="description"><a class="button" href="#" id="ethpress_opensea_add">' . esc_html__( 'Add NFT', 'ethpress_opensea' ) . '</a></span>';
		echo '</p>';
		echo '</div>';
	}

	/**
	 * Saves NFT fields from product data.
	 *
	 * @since 0.1.0
	 *
	 * @param int $post_id Id if product.
	 */
	public static function woocommerce_process_product_meta( $post_id ) {
		// phpcs:disable WordPress.Security.NonceVerification.Missing -- wc does that.
		$next_meta = array(
			'redirect_url'       => sanitize_text_field(
				wp_unslash( isset( $_POST['ethpress_opensea_redirect_url'] ) ? $_POST['ethpress_opensea_redirect_url'] : '' )
			),
			'contract_addresses' => sanitize_text_field(
				wp_unslash( isset( $_POST['ethpress_opensea_contract_addresses'] ) ? $_POST['ethpress_opensea_contract_addresses'] : '' )
			),
			'token_ids'          => sanitize_text_field(
				wp_unslash( isset( $_POST['ethpress_opensea_token_ids'] ) ? $_POST['ethpress_opensea_token_ids'] : '' )
			),
		);
		// phpcs:enable WordPress.Security.NonceVerification.Missing
		update_post_meta(
			$post_id,
			'ethpress_opensea_product_data',
			$next_meta
		);
	}

	/**
	 * Restrict content from users without NFTs.
	 *
	 * @since 0.1.0
	 *
	 * @param string $content Contet of post.
	 * @return string Content.
	 */
	public static function the_content( $content ) {
		if ( ! is_single() || 'product' !== get_post_type() ) {
			return $content;
		}
		$meta = get_post_meta( get_the_ID(), 'ethpress_opensea_product_data', true );
		if ( empty( $meta ) || empty( $meta['contract_addresses'] ) ) {
			return $content;
		}
		$user_id = get_current_user_id();
		if ( ! $user_id ) {
			return self::restricting_content();
		}
		$address = Address::find_by_user( $user_id );
		if ( is_wp_error( $address ) ) {
			return self::restricting_content();
		}

		$contracts = array_filter( array_map( 'trim', explode( ',', $meta['contract_addresses'] ) ) );
		$tokens    = array_filter( array_map( 'trim', explode( ',', $meta['token_ids'] ) ) );

		$ownerships = Opensea::get_ownerships(
			$address->get_coinbase(),
			$contracts,
			$tokens,
			1
		);

		if ( ! empty( $ownerships['assets'] ) && 0 < count( $ownerships['assets'] ) ) {
			// The user owns a token on the list.
			return $content;
		}
		return self::restricting_content();
	}

	/**
	 * Outputs the content you see when NFT is not owned.
	 *
	 * @since 0.1.0
	 */
	public static function restricting_content() {
		$meta    = get_post_meta( get_the_ID(), 'ethpress_opensea_product_data', true );
		$content = '';

		if ( ! empty( $meta ) && ! empty( $meta['redirect_url'] ) ) {
			$content .= '<p>' . esc_html__( 'You are being automatically redirected to the NFT page.', 'ethpress_opensea' ) . '</p>';
			$content .= '<p><a href="' . esc_url( $meta['redirect_url'] ) . '">' . esc_html__( 'Click here if you are not.', 'ethpress_opensea' ) . '</a></p>';
			$content .= '<script>window.location = ' . wp_json_encode( esc_url( $meta['redirect_url'] ) ) . '</script>';
		}
		$content .= '<p>' . esc_html__( 'An NFT is Required to View This Content.', 'ethpress_opensea' ) . '</p>';
		return apply_filters( 'ethpress_opensea_restricting_content', $content );
	}

	/**
	 * Check nft ownership and return something based on conditions.
	 *
	 * @since 0.1.0
	 *
	 * @param WC_Product $product WooCommerce product.
	 * @param array      $return_values What to return. [ 'not_logged_in', 'no_nft_data', 'does_own_nft', 'does_not_own_nft' ].
	 * @return mixed One of $return_values.
	 */
	public static function has_nft( $product, $return_values ) {
		$meta = $product->get_meta( 'ethpress_opensea_product_data' );
		if ( empty( $meta['contract_addresses'] ) ) {
			return $return_values['no_nft_data'];
		}
		$user_id = get_current_user_id();
		if ( ! $user_id ) {
			return $return_values['not_logged_in'];
		}

		$ownerships = array();
		// Let's get all the ownerships first, hope we luck out.
		if ( ! isset( self::$ownerships['user'] ) ) {
			// We do not want to store in user meta, since user can sell a token,
			// After which we do not want it to show up for them.
			$user_id = get_current_user_id();
			if ( $user_id ) {
				$address = Address::find_by_user( $user_id );
				if ( ! is_wp_error( $address ) ) {
					$ownerships = Opensea::get_ownerships(
						$address->get_coinbase()
					);
					if ( isset( $ownerships['assets'] ) ) {
						$ownerships               = $ownerships['assets'];
						self::$ownerships['user'] = $ownerships;
					}
				}
			}
		}
		if ( isset( self::$ownerships['user'] ) ) {
			$ownerships = self::$ownerships['user'];
		}

		$contracts = array_filter( array_map( 'trim', explode( ',', $meta['contract_addresses'] ) ) );
		$tokens    = array_filter( array_map( 'trim', explode( ',', $meta['token_ids'] ) ) );

		foreach ( $ownerships as $ownership ) {
			if (
				! empty( $ownership['asset_contract'] )
				&& in_array( $ownership['asset_contract']['address'], $contracts, true )
				&& in_array( $ownership['token_id'], $tokens, true )
			) {
				return $return_values['does_own_nft'];
			}
		}

		// We checked them all. It just isn't there. The limit of the query is 50, so if it's below that,
		// then those were all the NFTs that the user owns.
		if ( 50 > count( $ownerships ) ) {
			return $return_values['does_not_own_nft'];
		}

		// Okay, it was not found on the big query. Let's be product specific.
		$product_id = (string) $product->get_id();
		$ownerships = array();

		// Have we already gone through this product? Use the cache, then.
		// The cache is refreshed on every query, so if we are using it, then trying again isn't going to change anything.
		if ( isset( self::$ownerships[ $product_id ] ) ) {
			$ownerships = self::$ownerships[ $product_id ];
		} else {
			$address = Address::find_by_user( $user_id );
			if ( ! $address || is_wp_error( $address ) ) {
				return $return_values['does_not_own_nft'];
			}
			$ownerships = Opensea::get_ownerships(
				$address->get_coinbase(),
				$contracts,
				$tokens,
				1
			);
			if ( empty( $ownerships['assets'] ) ) {
				$ownerships = array();
			} else {
				$ownerships = $ownerships['assets'];
			}
			// Cache this result.
			self::$ownerships[ $product_id ] = $ownerships;
		}

		// We don't need to check, really, but let's do anyways. Safe than sorry.
		foreach ( $ownerships as $ownership ) {
			if (
				! empty( $ownership['asset_contract'] )
				&& in_array( $ownership['asset_contract']['address'], $contracts, true )
				&& in_array( $ownership['token_id'], $tokens, true )
			) {
				return $return_values['does_own_nft'];
			}
		}
		return $return_values['does_not_own_nft'];
	}

	/**
	 * Load translation files.
	 *
	 * @since 1.1.0
	 */
	public static function load_plugin_textdomain() {
		$path  = dirname( plugin_basename( ETHPRESS_OPENSEA_FILE ) );
		$path .= '/languages';
		load_plugin_textdomain( 'ethpress_opensea', false, $path );
	}

}
