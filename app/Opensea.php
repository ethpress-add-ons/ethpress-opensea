<?php
/**
 * OpenSea API
 *
 * @package Ethpress_Opensea
 *
 * @since 0.1.0
 */

namespace losnappas\Ethpress_Opensea;

defined( 'ABSPATH' ) || die;

/**
 * Wraps opensea api calls.
 *
 * @since 0.1.0
 */
class Opensea {
	/**
	 * Gets asset ownerships.
	 *
	 * @since 0.1.0
	 *
	 * @param string $coinbase User's wallet address.
	 * @param array  $contract_addresses Optional. Contract addresses.
	 * @param array  $token_ids Optional. Token ids.
	 * @param int    $limit Optional. Limit.
	 *
	 * @return (array|bool) Data or false.
	 */
	public static function get_ownerships( $coinbase, $contract_addresses = array(), $token_ids = array(), $limit = 50 ) {
		if ( empty( $coinbase ) ) {
			return false;
		}
		$url = 'https://api.opensea.io/api/v1/assets';

		$url = add_query_arg( 'owner', $coinbase, $url );
		$url = add_query_arg( 'limit', $limit, $url );
		foreach ( $token_ids as $n => $token_id ) {
			$caddr = $contract_addresses[ $n ];
			if ( ! is_numeric( $token_id ) || ! $caddr ) {
				continue;
			}
			$url .= '&token_ids=' . $token_id;
			$url .= '&asset_contract_addresses=' . $caddr;
		}
		$args = array();
		$opt  = get_option( 'ethpress_opensea', array() );
		if ( ! empty( $opt['opensea_api_key'] ) ) {
			$args = array(
				'headers' => array(
					'X-API-KEY' => $opt['opensea_api_key'],
				),
			);
		}
		$response = wp_safe_remote_get(
			$url,
			$args
		);

		if ( is_wp_error( $response ) ) {
			return false;
		}
		$body = wp_remote_retrieve_body( $response );
		if ( ! empty( $body ) ) {
			$json = json_decode( $body, true );
			return $json;
		} else {
			return false;
		}
	}
}
