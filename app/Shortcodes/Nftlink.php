<?php
/**
 * Shortcode for NFTs
 *
 * @since 0.5.0
 *
 * @package Ethpress_Opensea
 */

namespace losnappas\Ethpress_Opensea\Shortcodes;

defined( 'ABSPATH' ) || die;

use losnappas\Ethpress_Opensea\Plugin;

/**
 * Contrainer class for shortcode
 *
 * @since 0.5.0
 */
class Nftlink {
	/**
	 * Creates shortcode ethpress_opensea_nft. Runs on `\add_shortcode`.
	 *
	 * Outputs nothing when user is logged in. Button otherwise.
	 *
	 * @since 0.5.0
	 * @param array $atts Has int $product_id Product id.
	 * @return string Stuff.
	 */
	public static function add_shortcode( $atts ) {
		$product = wc_get_product( (int) $atts['product_id'] );
		if ( ! $product ) {
			return '<p>' . esc_html__( 'No product with that ID.', 'ethpress_opensea' ) . '</p>';
		}
		if ( ! is_user_logged_in() ) {
			wp_enqueue_script( 'ethpress-login-front' );
		}
		$meta = $product->get_meta( 'ethpress_opensea_product_data' );
		$url  = '#';
		if ( ! empty( $meta['redirect_url'] ) ) {
			$url = $meta['redirect_url'];
		}
		return Plugin::has_nft(
			$product,
			array(
				'not_logged_in'    => '<p><a href="' . esc_url( $product->get_permalink() ) . '" class="button ethpress-opensea-nft ethpress-metamask-login-button">' . esc_html__( 'Check for NFT to Access', 'ethpress_opensea' ) . '</a></p>',
				'no_nft_data'      => '<p><a href="' . esc_url( $product->get_permalink() ) . '" class="button ethpress-opensea-nft no-nft-data">' . esc_html__( 'View Product', 'ethpress_opensea' ) . '</a></p>',
				'does_own_nft'     => '<p><a href="' . esc_url( $product->get_permalink() ) . '" class="button ethpress-opensea-nft no-nft-data">' . esc_html__( 'View Product', 'ethpress_opensea' ) . '</a></p>',
				'does_not_own_nft' => '<p><a href="' . esc_url( $url ) . '" class="button ethpress-opensea-nft no-nft-data">' . esc_html__( 'Purchase NFT to Access', 'ethpress_opensea' ) . '</a></p>',
			)
		);
	}
}
