<?php
/**
 * Displays options on EthPress' options page.
 *
 * @since 0.1.0
 *
 * @package EthPress_Opensea
 */

namespace losnappas\Ethpress_Opensea\Admin;

defined( 'ABSPATH' ) || die;

/**
 * Static.
 *
 * @since 0.1.0
 */
class Options {

	/**
	 * Adds options page. Attached to hook.
	 *
	 * @since 0.1.0
	 */
	public static function admin_menu() {
		$page = esc_html__( 'EthPress Opensea', 'ethpress_opensea' );
		add_options_page(
			$page,
			$page,
			'manage_options',
			'ethpress_opensea',
			array( __CLASS__, 'create_page' )
		);
	}

	/**
	 * Creates options page.
	 *
	 * @since 0.1.0
	 */
	public static function create_page() {
		?>
		<div class="wrap">
			<h1><?php esc_html_e( 'EthPress Opensea', 'ethpress_opensea' ); ?></h1>
			<p>[ethpress_opensea_nft product_id="1337"] -- shortcode available.</p>
			<form action="options.php" method="POST">
			<?php
			settings_fields( 'ethpress_opensea' );
			do_settings_sections( 'ethpress_opensea' );
			submit_button();
			?>
			</form>
		</div>
		<?php
	}

	/**
	 * Adds settings for api_url to options page. admin_init hooked.
	 *
	 * @since 0.1.0
	 */
	public static function admin_init() {
		register_setting(
			'ethpress_opensea',
			'ethpress_opensea',
			array( __CLASS__, 'options_validate' )
		);

		add_settings_section(
			'ethpress_opensea_main',
			esc_html__( 'EthPress NFT OpenSea', 'ethpress_opensea' ),
			array( __CLASS__, 'settings_section' ),
			'ethpress_opensea'
		);
		add_settings_field(
			'ethpress_opensea_options',
			esc_html__( 'Settings', 'ethpress_opensea' ),
			array( __CLASS__, 'settings_field' ),
			'ethpress_opensea',
			'ethpress_opensea_main'
		);
	}

	/**
	 * Outputs section title.
	 *
	 * @since 0.1.0
	 */
	public static function settings_section() {
	}

	/**
	 * Outputs options.
	 *
	 * @since 0.1.0
	 */
	public static function settings_field() {
		$options = get_option(
			'ethpress_opensea',
			array(
				'opensea_api_key' => '',
			)
		);
		?>
		<h4><?php esc_html_e( 'API key from https://opensea.io', 'ethpress_opensea' ); ?></h4>
		<input id="ethpress_opensea_opensea_api_key" name="ethpress_opensea[opensea_api_key]" placeholder="<?php esc_attr_e( 'API key', 'ethpress_opensea' ); ?>" type="text" class="regular-text" value="<?php echo esc_attr( $options['opensea_api_key'] ); ?>">

		<?php
	}

	/**
	 * Validates input for api url option.
	 *
	 * @param array $input New options input.
	 *
	 * @since 0.1.0
	 */
	public static function options_validate( $input ) {
		$opts                    = get_option( 'ethpress_opensea', array() );
		$opts['opensea_api_key'] = trim( sanitize_text_field( $input['opensea_api_key'] ) );
		return $opts;
	}


	/**
	 * Adds settings link. Hooked to filter.
	 *
	 * @since 0.1.0
	 *
	 * @param array $links Existing links.
	 */
	public static function plugin_action_links( $links ) {
		$url           = esc_attr(
			esc_url(
				add_query_arg(
					'page',
					'ethpress_opensea',
					get_admin_url() . 'options-general.php'
				)
			)
		);
		$label         = esc_html__( 'Settings', 'ethpress_opensea' );
		$settings_link = "<a href='$url'>$label</a>";

		array_unshift( $links, $settings_link );
		return $links;
	}
}

