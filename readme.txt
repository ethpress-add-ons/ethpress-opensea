=== Ethpress OpenSea ===
Contributors: lynn999
Donate link: https://www.crowdrise.com/horn-of-africa/fundraiser/lynn99
Tags: ethpress, token, ownership, opensea
Requires at least: 4.6
Tested up to: 5.5
Stable tag: 1.0.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Premium EthPress add-on. Check user NFT (non-fungible token, erc-721 and erc-1155) ownership. Perfect for blocking users access to a Product page, if they don't own a certain token.

== Description ==

Adds Ethereum NFT integration with WooCommerce products, via [OpenSea](https://opensea.io) and [EthPress](https://wordpress.org/plugins/ethpress/).

Start off by configuring this add-on in your WordPress admin by inputting your,

1. API key for opensea.io (free, https://docs.opensea.io/reference#request-an-api-key (scroll to bottom)).
2. Your tokens' contract addresses and ids on the WC Product pages, into NFT product data tab.

== Installation ==

Download the zip from TODO .

Install it via WordPress plugin installer by clicking on "Add New" and then on "Upload Plugin".

== Frequently Asked Questions ==

== Screenshots ==

== Changelog ==

= 1.0.0 =

* Initial release 9/2020.

== Upgrade Notice ==
